#! /bin/bash
#  unflat - Hierarchical Arrangement Script for the danbooru data folder
#  Version 1.1
#  (hopefully the only one OwO)
#
#  Author: Glassed Silver
#  Contributor: Alessio Vanni

#  Welcoming the user and disclaimer
cat <<EOF
unflat - Hierarchical Arrangement Script for the danbooru data folder 
Version 1.1
Author: Glassed Silver
Contributor: Alessio Vanni

This is a shell script and the methodical alternative to the fix
script 77, only that this script doesn't intend to create any
symlinks, but rather move all files in the root data folder and its
sub- directories crop, sample and preview to the non-flat,
hierarchical file structure, that is now the only way danbooru stores
files going forward.

  IMPORTANT: MAKE SURE YOU BACKED UP YOUR DATA FOLDER and that you run
  this script as the danbooru user!  I take no responsibility for
  anything that gets borked or breaks from this, so be cautious! Run
  this INSTEAD of 077_symlink_subdirectories.rb!



EOF

#  Prompt to continue
read -p "Press enter to continue (ctrl+c to cancel)"
printf "\nPreparing... \n"

if [ -z "$1" ] || ! [ -d "$1" ]; then
    echo 'Missing data directory (pass it as a command line argument)'
    exit 1
fi

if [[ -L "$1" ]]; then
    echo "$1 is a symbolic link. Please use the real path"
    exit 1
fi

cd "$1"
mkdir ../data-processing
if ! [[ -d ../data-processing ]]; then
    echo "unable to set up environment"
    exit 1
fi

mv crop/ ../data-processing/crop
mv sample/ ../data-processing/sample
mv preview/ ../data-processing/preview

find . -type f -exec mv {} ../data-processing \;
mkdir original

mv ../data-processing/crop crop/
mv ../data-processing/sample sample/
mv ../data-processing/preview preview/

cd ../data-processing/

cat <<EOF 
Beginning now
Stage 1 - $1 (originals are in original/ going forward)
EOF

for f in *; do if [ ! -d "$f" ]; then d="${f:0:2}"; mkdir -p "$d"; mv -t "$d" -- "$f"; fi; done
for dir in ./*; do (cd "$dir" && for f in *; do if [ ! -d "$f" ]; then d="${f:2:2}"; mkdir -p "$d"; mv -t "$d" -- "$f"; fi; done); done
cd ..
mv * data/original/
rmdir data-processing
cd data/

echo "Stage 2 - $1/crop"

cd crop/
for f in *; do if [ ! -d "$f" ]; then d="${f:0:2}"; mkdir -p "$d"; mv -t "$d" -- "$f"; fi; done
for dir in ./*; do (cd "$dir" && for f in *; do if [ ! -d "$f" ]; then d="${f:2:2}"; mkdir -p "$d"; mv -t "$d" -- "$f"; fi; done); done
cd ..

echo "Stage 3 - $1/preview \n"

cd preview/
for f in *; do if [ ! -d "$f" ]; then d="${f:0:2}"; mkdir -p "$d"; mv -t "$d" -- "$f"; fi; done
for dir in ./*; do (cd "$dir" && for f in *; do if [ ! -d "$f" ]; then d="${f:2:2}"; mkdir -p "$d"; mv -t "$d" -- "$f"; fi; done); done
cd ..

echo "Stage 4 - $1/sample \n\n"

cd sample/
for f in *; do if [ ! -d "$f" ]; then d="${f:7:2}"; mkdir -p "$d"; mv -t "$d" -- "$f"; fi; done
for dir in ./*; do (cd "$dir" && for f in *; do if [ ! -d "$f" ]; then d="${f:9:2}"; mkdir -p "$d"; mv -t "$d" -- "$f"; fi; done); done
cd ..

printf "DONE \nEnjoy your day! :) \n\n"
