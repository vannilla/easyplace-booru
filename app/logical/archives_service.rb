require 'socket'

class ArchivesService
  def self.send_message(string)
    port = Danbooru.config.archives_local_port
    conn = TCPSocket.new('localhost', port)
    conn.write(string)
  end
end
