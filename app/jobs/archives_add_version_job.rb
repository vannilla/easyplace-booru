class ArchivesAddVersionJob < ApplicationJob
  def perform(data)
    ArchivesService.send_message(data)
  end
end
